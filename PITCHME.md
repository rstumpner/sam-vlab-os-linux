## Systemadministraton 2 Übung
* SAM 2 Übung
* OS

---
## Linux (Ubuntu)
* OS - Linux (Ubuntu)
* Version 2020308

---
## Agenda
* Einführung Linux Ubuntu
* Voraussetzungen vLAB
* Umgebung vLAB
* Installation Linux (Ubuntu)
* First Steps
* Basics

---
## Einführung Linux

#### Als Linux oder GNU/Linux bezeichnet man in der Regel freie, unixähnliche Mehrbenutzer-Betriebssysteme, die auf dem Linux-Kernel und wesentlich auf GNU-Software basieren.

(https://de.wikipedia.org/wiki/Linux)

---
## Einführung Ubuntu

#### Ubuntu, auch Ubuntu Linux, ist eine Linux-Distribution, die auf Debian basiert. Der Name Ubuntu bedeutet auf Zulu etwa „Menschlichkeit“ und bezeichnet eine afrikanische Philosophie. Die Entwickler verfolgen mit dem System das Ziel, ein einfach zu installierendes und leicht zu bedienendes Betriebssystem mit aufeinander abgestimmter Software zu schaffen. Das Projekt wird vom Software-Hersteller Canonical gesponsert, der vom südafrikanischen Unternehmer Mark Shuttleworth gegründet wurde

(https://de.wikipedia.org/wiki/Ubuntu)

---
## Voraussetzungen vLAB
* VM ( Virtualbox / Vmware Workstation / Proxmox VM)
* 1 x CPU Core
* 1 GB RAM
* Linux Ubuntu 18.04 LTS

---
## Installation vLAB
* Installation vLAB Manual
* Installation vLAB Vagrant

---
## Installation Linux Ubuntu (Manual)
* Ubuntu 18.04 LTS Basis

---
## Installation Linux Ubuntu (Vagrant)
* git clone https://www.gitlab.com/rstumper/sam-vlab-os-linux
* cd vlab/vagrant-virtualbox/
* vagrant up
* vagrant ssh
* Username: vagrant
* Password: vagrant

---
## Troubleshooting Hyper-V
#### Um Vagrant mit VirtualBox und Installiertem Hyper-V zu betreiben ist es notwendig Hyper-V zu deaktivieren.

* Turn Hyper-V OFF
```
bcdedit /set hypervisorlaunchtype off
```

* Turn Hyper-V ON
```
bcdedit /set hypervisorlaunchtype auto
```

---
## First Steps in Linux
* Check Puppet Agent Version:
	* /opt/puppetlabs/bin/puppet agent --version

---
## Basics (Looking Around)
* pwd
* cd
* ls
* df

---
## Basics (Files)
* touch
* cat
* grep
* more
* less
* echo


---
## Cheat Sheet Linux
* Installation (sudo apt-get install snapd snap-confine )
* Snap Version (sudo snap --version)
* Snap Package Suchen ( snap find )
* Snap Package Installieren (sudo snap install )
* snap list
* snap refresh
* snap remove
* snap info

---
## Links
* https://wiki.ubuntuusers.de/snap/
* https://snapcraft.io/docs/core/usage
* https://uappexplorer.com
* https://www.ubuntu.com/desktop/snappy

---
## Begriffserklärungen
* GNU
* OSS
---
## Notizen
