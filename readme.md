# SAM vLab Linux Basics
This is a Gitlab Repository to do an easy Virtual Lab Environment for Learning Linux Basics ( https://de.wikipedia.org/wiki/Linux ) with an Ubuntu Open Source Distribution.

#### Follow the Instructions of this Virtual Lab:

Option 1 (Online):
  * Browse https://gitpitch.com/rstumpner/sam-vlab-os-linux/master?grs=gitlab&t=simple

Option 2 (Local):
  * git clone https://gitlab.com/rstumpner/sam-vlab-os-linux
  * gem install showoff
  * showoff serve
  * Browse http://localhost:9090

#### Setup the Virtual Lab Environment

Pre-Requirements:
  * 1 GB Memory (minimal)
  * 1 x CPU Cores
  * Installation of Virtualbox (https://www.virtualbox.org/)

The vLAB Environment:
  * ubuntu (Ubuntu 18.04)

Automatic Setup with Vagrant (https://www.vagrantup.com/) (local):
  * Download and Install Vagrant Package for your OS
    * On Linux
      * wget https://releases.hashicorp.com/vagrant/2.0.3/vagrant_2.0.3_x86_64.deb?_ga=2.257941326.439840422.1522128825-1814262215.1522128825
      * dpkg -i vagrant_2.0.3_x86_64.deb
    * On Windows
      * https://releases.hashicorp.com/vagrant/2.0.3/vagrant_2.0.3_x86_64.msi?_ga=2.89037278.439840422.1522128825-1814262215.1522128825
    * on macOS
      * https://releases.hashicorp.com/vagrant/2.0.3/vagrant_2.0.3_x86_64.dmg?_ga=2.257941326.439840422.1522128825-1814262215.1522128825

  * Clone this Git Repository
    * git clone https://gitlab.com/rstumpner/sam-vlab-os-linux
  * Start the Setup the vLAB Environment with Vagrant
    ```md
       cd sam-vlab-os-linux/vlab/vagrant-virtualbox/
       vagrant up
       ```
  * Check the vLAB Setup
     ```md
     vagrant status
     ```
  * Login to work with a Node
    ```md
    vagrant ssh
    ```
#### Troubleshooting
#### Hyper-V
Um Vagrant mit VirtualBox und Installiertem Hyper-V zu betreiben ist es notwendig Hyper-V zu deaktivieren.

* Turn Hyper-V OFF
```
bcdedit /set hypervisorlaunchtype off
```

* Turn Hyper-V ON
```
bcdedit /set hypervisorlaunchtype auto
```
