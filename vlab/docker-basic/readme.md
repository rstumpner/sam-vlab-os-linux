<!-- $size: 16:9 -->
<!-- page_number: true -->
<!-- footer: Roland Stumpner GPLv3 -->


# Systemadministraton 2 Übung Docker Setup
* Ubuntu 16.04 LTS Basis
* sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
* echo "deb https://apt.dockerproject.org/repo ubuntu-xenial main" | sudo tee /etc/apt/sources.list.d/docker.list
* apt-get update
* Install Optimierte Kernel Erweiterungen sudo apt-get install linux-image-extra-$(uname -r) linux-image-extra-virtual
* sudo apt-get install docker-engine
* Enable Docker Startup sudo systemctl enable docker

---

# Alternative Installation
* sudo su
* wget -qO- https://get.docker.com/ | sh

---
# Installation SAM vLAB Powercli (Manual)
* Start the latest Ubuntu Container
```md
sudo docker run -t -i ubuntu:latest /bin/bash
```
* Install the latest Powershell 
* Install VMware PowerCLI

---
# Installation SAM vLAB Powercli (Dockerfile)
* Get Powercli Docker Container File from Github
* Start the Docker Container

